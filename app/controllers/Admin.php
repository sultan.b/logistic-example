<?php
    class Admin extends Controller 
    {
        public function __construct()
        {
            $this->adminModel = $this->model('Admins');
            $this->userModel = $this->model('User');
        }
        
        public function index()
        {
            $this->view('admin/index');
        }

        public function edit_user()
        {

        }

        public function orders()
        {
            $var = $this->adminModel->getOrders();
            $data = [
                'orders'        => $var,
            ];
            $this->view('admin/orders', $data);
        }
        
        public function users()
        {
            $users = $this->adminModel->getUsers();
            $data = [
                'users'         => $users,
            ];
            $this->view('admin/users', $data);
        }

        public function editUser($id)
        {
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $data = [
                    'id'        => $id,
                    'f_name'    => $_POST['f_name'],
                    'l_name'    => $_POST['l_name'],
                    'address'   => $_POST['address'],
                ];

                $isUpdated = $this->adminModel->updateUser($data);
                
                if ($isUpdated) {
                    redirect('admin/users');
                } else {
                    redirect('admin');
                }
            }
            else
            {
                $user = $this->userModel->getUserById($id);
                $data = [
                    'id'          => $user->id,
                    'f_name'      => $user->f_name,
                    'l_name'      => $user->l_name,
                    'address'     => $user->address,
                ];
                $this->view('admin/edit-user', $data);
            }
        }

        public function deleteUser($id)
        {
            # code...
        }

        public function transports()
        {
            
        }

        public function roles()
        {
            $result = $this->adminModel->getRoles();
            $data = [
                'roles'     => $result,
            ];
            $this->view('admin/roles', $data);
        }

        public function addroles()
        {
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $data = [
                    'role_name'     => $_POST['role_name'],
                ];
                if($this->adminModel->addRole($data))
                {
                    redirect('admin/roles');
                }
            }else
            {
                $this->view('admin/add-roles');
            }
            
        }

        public function editRole($id)
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $data = [
                    'id'            => $id,
                    'role_name'     => $_POST['role_name'],
                    'role_err'      => '',
                ];
                if ($this->adminModel->editRole($data)) {
                    redirect('admin/roles');
                } else {
                    die("error");
                    //$this->view('admin/edit-role', $data);
                }
                
            } else {
                $role = $this->adminModel->getRoleById($id);
                $data = [
                    'id'            => $role->id,
                    'role_name'     => $role->role_name,
                    'role_err'      => '',
                ];
                $this->view('admin/edit-role', $data);
            }
        }

        public function deleteRoles($id)
        {
            $this->adminModel->deleteRoles($id);
            redirect('admin/roles');
        }

        public function drivers()
        {
            $drivers = $this->adminModel->getDrivers();
            $data = [
                'drivers'   => $drivers,
            ];
            $this->view('admin/drivers', $data);
        }

        public function transportTypes()
        {
            $types = $this->adminModel->getTransportTypes();
            $data = [
                't_types'               => $types,
            ];
            $this->view('admin/transport-types', $data);
        }

        public function subTypes($id)
        {
            $subtypes = $this->adminModel->getSubTypesById($id);
            $type = $this->adminModel->getTransportTypeById($id);
            $data = [
                'subtypes'             => $subtypes,
                'type'                 => $type,
            ];

            $this->view('admin/transport-subtypes', $data);
        }

        public function addSubTypes($typeId)
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $data = [
                    'transport_name'            => trim($_POST['transport_name']),
                    'transport_type_id'         => $_POST['transport_type_id'],
                    'transport_name_err'        => '',
                ];

                if (empty($data['transport_name'])) {
                    $data['transport_name_err'] = 'Введите название подтипа';
                }
                if (empty($data['transport_name_err'])) {
                    if ($this->adminModel->addSubType($data)) {
                        redirect('admin/subtypes/'. $typeId);
                    }
                } else {
                    $this->view('admin/addsubtypes', $data);
                }
            } else {
                $data = [
                    'typeId'           => $typeId,
                ];
                $this->view('admin/addsubtypes', $data);
            }
        }

        public function deleteSubType($id)
        {
            $this->adminModel->deleteSubType($id);
            redirect('admin/subtypes');
        }

        public function getSubTypesById($id){
            $row = $this->adminModel->getSubTypesById($id);
            if(empty($row)){
                ret(json_encode($id));
            }else{
                $data = [
                    'subTypes'          => $row,
                ];
                $ret = array_values($row);
                ret(json_encode($ret, JSON_UNESCAPED_UNICODE));
            }
        }

        public function faq()
        {
            $faq = $this->adminModel->getFaq();
            $data = [
                'faq'               => $faq,
            ];
            $this->view('admin/faq', $data);
        }

        public function addFaq()
        {
            $data = [
                'question'          => trim($_POST['question']),
                'answer'            => trim($_POST['answer']),
                'for_client'        => $_POST['for_client']
            ];
            
            if ($this->adminModel->addFaq($data)) {
                return ret(json_encode(1, JSON_UNESCAPED_UNICODE));
            } else {
                return ret(json_encode(0, JSON_UNESCAPED_UNICODE));
            }
            
        }

        public function updateFaq()
        {
            $data = [
                'id'                => $_POST['id'],
                'question'          => trim($_POST['question']),
                'answer'            => trim($_POST['answer']),
                'for_client'        => $_POST['for_client'] != null ? $_POST['for_client'] : 0,
            ];
            if ($this->adminModel->UpdateFaQ($data)) {
                return ret(json_encode(1, JSON_UNESCAPED_UNICODE));
            } else {
                return ret(json_encode(0, JSON_UNESCAPED_UNICODE));
            }
        }

        public function deleteFaq($id)
        {
            $var = [    
                'isSuccess'         => ''
            ];
            if ($this->adminModel->RemoveFaq($id)) {
                $var['isSuccess'] = true;
                return ret(json_encode($var, JSON_UNESCAPED_UNICODE));
            } else {
                $var['isSuccess'] = false;
                return ret(json_encode($var, JSON_UNESCAPED_UNICODE));
            }
        }

        public function getFaqById($id)
        {
            $ret = $this->adminModel->getFaqById($id);
            $data = array(
                'faq'          => $ret
            );
            $var = array(
                'faq' => $ret
            );
            ret(json_encode($var, JSON_UNESCAPED_UNICODE));
        }
    }
