<?php 
	class Pages extends Controller {
		public function __construct(){
			$this->bookModel = $this->model('Book');
			$this->userModel = $this->model('User');
			$this->adminModel = $this->model('Admins');
		}

		public function index(){
			$user = $this->userModel->checkUserType($_SESSION['user_id']);
			$data = [
				'books' 	 => '',
				'genresList' => '',
				'user'		 => $user,
			];
			$this->view('pages/index', $data);
		}

		public function about(){
			$data = [
				// 'title'=> 'About US'
			];
			$this->view('pages/about', $data);
		}

		public function faq()
		{
			$clientFaq = $this->adminModel->getClientFaq();
			$driverFaq = $this->adminModel->getDriverFaq();
			$data = [
				'clientFaq' 		=> $clientFaq,
				'driverFaq' 		=> $driverFaq,
			];
			$this->view('pages/faq', $data);
		}

		public function contact(){
			$this->view('pages/contact');
		}
	}
 ?>