<?php
    class Client extends Controller
    {
        public function __construct()
        {
            $this->adminModel = $this->model('Admins');
            $this->userModel = $this->model('User');
        }

        public function index()
        {
            $this->view('client/index');
        }

        public function orders()
        {
            $this->view('client/orders');
        }

        public function my_orders()
        {
            $this->view('client/my_orders');
        }

        public function add_order()
        {

        }

        public function taxi()
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $data = [
                    'first_address'             => $_POST['first_address'],
                    'last_address'              => $_POST['last_address'],
                    'price'                     => $_POST['price'],
                    'comment'                   => $_POST['comment'],
                    'order_type_id'             => $_POST['order_type_id'],
                    'user_id'                   => $_SESSION['user_id'],
                    'is_accepted'               => 0
                ];
                $orderId = $this->adminModel->addClientOrder($data);
                $res = [
                    'order_id'     => '',
                    'qwerty'       => $orderId,
                ];
                if ($orderId != null) {
                    $res['order_id'] = $orderId;
                    $var = array($res);
                    ret(json_encode($var, JSON_UNESCAPED_UNICODE));
                } else {
                    ret(json_encode($res, JSON_UNESCAPED_UNICODE));
                    //$this->view('admin/edit-role', $data);
                }
            } else {
                $history = $this->adminModel->getTaxiOrdersByClientId($_SESSION['user_id']);
                $data = [
                    'history'           => $history
                ];
                $this->view('client/taxi', $data);
            }
        }
        
        public function getAcceptedOrder($id)
        {
            $accept = $this->adminModel->getAcceptedOrderByUserId($_SESSION['user_id'], $id);
            $data = [
                'accepted'          => ''
            ];

            if ($accept != null) {
                $data['accepted'] = $accept;
            } else {
                $data['accepted'] = null;
            }

            $var = array($data);
            return ret(json_encode($var, JSON_UNESCAPED_UNICODE));
        }

        public function freight()
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $data = [
                    'first_address'             => $_POST['first_address'],
                    'last_address'              => $_POST['last_address'],
                    'price'                     => $_POST['price'],
                    'comment'                   => $_POST['comment'],
                    'start_date'                   => $_POST['start_date'],
                    'order_type_id'             => $_POST['order_type_id'],
                    'user_id'                   => $_SESSION['user_id'],
                    'is_accepted'               => 0
                ];
                $orderId = $this->adminModel->addClientOrder($data);
                $res = [
                    'order_id'     => '',
                ];
                if ($orderId != null) {
                    $res['order_id'] = $orderId;
                    $var = array($res);
                    ret(json_encode($var, JSON_UNESCAPED_UNICODE));
                } else {
                    ret(json_encode($res, JSON_UNESCAPED_UNICODE));
                }
            } else {
                $freightOrders = $this->adminModel->getFreightOrdersByClientId($_SESSION['user_id']);
                $data = [
                    'freightOrders'         => $freightOrders,
                ];
                $this->view('client/freight', $data);
            }
        }

        public function other()
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $data = [
                    'freightOrders'             => '',
                    'work_title'                => $_POST['work_title'],
                    'first_address'             => $_POST['first_address'],
                    'comment'                   => $_POST['comment'],
                    'order_type_id'             => $_POST['order_type_id'],
                    'order_subtype_id'          => $_POST['order_subtype_id'],
                    'date_start'                => $_POST['date_start'],
                    'user_id'                   => $_SESSION['user_id'],
                    'price'                     => $_POST['price'],
                    'is_accepted'               => 0
                ];
                $var = [
                    'isSuccess'   => false
                ];
                if($this->adminModel->addOtherOrder($data)) {
                    $var['isSuccess'] = true;
                    ret(json_encode($var, JSON_UNESCAPED_UNICODE));
                }else{
                    ret(json_encode($var, JSON_UNESCAPED_UNICODE));
                }
            } else {
                $data = [
                    'subtypes'          => '',
                    'history'           => '',
                ];

                $subtypes = $this->adminModel->getSubtypesByTypeId(3);
                $history = $this->adminModel->getOtherOrdersByClientId($_SESSION['user_id']);
                if($subtypes != null){
                    $data['subtypes'] = $subtypes;
                    $data['history'] = $history;
                }
                $this->view('client/other', $data);
            }
        }

        public function currentOrder($id)
        {
            $data = [
                'order'                 => '',
                'acceptedOrderId'       => $id,
                //'orderId'               => $_POST['order_id'],
                //'driverId'              => $_POST['driver_id'],
                'user_id'               => $_SESSION['user_id']
            ];
            $getOrder = $this->adminModel->getClientOrderByOrderId($data);
            if($getOrder != null){
                $data['order'] = $getOrder;
            }
            $this->view('client/current-order', $data);
        }

        public function bus()
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $data = [
                    'first_address'             => $_POST['first_address'],
                    'last_address'              => $_POST['last_address'],
                    'price'                     => $_POST['price'],
                    'comment'                   => $_POST['comment'],
                    'start_date'                   => $_POST['start_date'],
                    'order_type_id'             => $_POST['order_type_id'],
                    'order_subtype_id'          => $_POST['order_subtype_id'],
                    'user_id'                   => $_SESSION['user_id'],
                    'is_accepted'               => 0
                ];
                $orderId = $this->adminModel->addBusOrder($data);
                $res = [
                    'order_id'     => '',
                ];
                if ($orderId != null) {
                    $res['order_id'] = $orderId;
                    $var = array($res);
                    ret(json_encode($var, JSON_UNESCAPED_UNICODE));
                } else {
                    ret(json_encode($res, JSON_UNESCAPED_UNICODE));
                }
            } else {
                $history = $this->adminModel->getBusOrdersByClientId($_SESSION['user_id']);
                $subtypes = $this->adminModel->getSubtypesByTypeId(4);
                $data = [
                    'subtypes'              => $subtypes,
                    'finishedOrders'        => $history,
                ];
                $this->view('client/bus', $data);
            } 
        }
    }
