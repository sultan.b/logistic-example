<?php
class Driver extends Controller
{
    public function __construct()
    {
        $this->adminModel = $this->model('Admins');
        $this->userModel = $this->model('User');
    }

    public function index()
    {
        $user = $this->userModel->checkUserType($_SESSION['user_id']);
        $isTransportAdded = $this->userModel->checkDriverTransport($user->id);
        $isDriverType = $this->userModel->driverTransportType($_SESSION['user_id']);
        $orders = $this->adminModel->getClientOrders($isDriverType->t_type_id);
        $data = [
            'user'          => $user,
            'isTransport'   => $isTransportAdded,
            'orders'        => $orders,
        ];
         $this->view('driver/index', $data);
    }

    public function orders()
    {
        # code...
        $this->view('driver/orders');
    }

    public function transport()
    {
        $transport = $this->adminModel->getDriverTransport($_SESSION['user_id']);
        $isTransportAdded = $this->userModel->checkDriverTransport($_SESSION['user_id']);
        $data = [
            'transport'         => $transport,
            'isAdded'           =>$isTransportAdded,
        ];
        $this->view('driver/transport', $data);
    }

    public function addTransport()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $subtype = empty($_POST['subtype_id']) ? NULL : $_POST['subtype_id'];
            $data = [
                't_name'                => trim($_POST['t_name']),
                't_state_number'        => trim($_POST['t_state_number']),
                't_type_id'             => $_POST['t_type_id'],
                'subtype_id'            => $subtype,
                'user_id'               => $_SESSION['user_id'],
            ];

            if($this->adminModel->addTransport($data)){
                redirect('driver/transport');
            }else{
                die("Error");
            }
        }else{
            $types = $this->adminModel->getTransportTypes();
            //$subTypes = $this->adminModel->getSubTypes();
            $data = [
                'types'             => $types,
            ];
            $this->view('driver/add-transport', $data);
        }
    }

    public function editTransport()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
                't_name'                => trim($_POST['t_name']),
                't_state_number'        => trim($_POST['t_state_number']),
                'user_id'               => $_SESSION['user_id'],
            ];

            if($this->adminModel->editTransport($data)){
                redirect('driver/transport');
            }else{
                die("Error");
            }
        }else{
            $transport = $this->adminModel->getDriverTransport($_SESSION['user_id']);
            $isTransportAdded = $this->userModel->checkDriverTransport($_SESSION['user_id']);
            $data = [
                'transport'         => $transport,
                'isAdded'           => $isTransportAdded,
            ];
            $this->view('driver/transport', $data);
        }
    }

    public function deleteTransport($id)
    {
        $this->adminModel->deleteTransport($id);
            redirect('driver/transport');        
    }

    public function acceptOrder()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $accept = $this->adminModel->addOrderToAccep($_POST['order_id'], $_POST['user_id']);
            $data = [
                'accepted'          => ''
            ];
    
            if($accept != null){
                $data['accepted'] = $accept;
            }else{
                $data['accepted'] = null;
            }
    
            $var = array($data);
            return ret(json_encode($var, JSON_UNESCAPED_UNICODE));
        }
    }
    
}
