<?php 
	// DB Params
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASS', '');
	define('DB_NAME', 'logistic');
	define('DB_CHAR', 'utf8mb4');
	// App Root
	define('APPROOT', dirname(dirname(__FILE__)));
	// Public Root
	define('PUBLICROOT', dirname(dirname(dirname(__FILE__))) . '\public');
	// URL Root
	define('URLROOT', 'http://trmvs');
	// Site Name
	define('SITENAME', "Логистика");
	// App Version
	define('APPVERSION', 'v1.0.0');
	// Set default timezone
	date_default_timezone_set("Asia/Almaty");
 ?>