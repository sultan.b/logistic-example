<?php
class User
{
	private $db;

	public function __construct()
	{
		$this->db = new Database();
	}

	// Register user
	public function register($data)
	{
		$this->db->query("INSERT INTO users (f_name, l_name, email, phone, password, user_role_id) VALUES (:f_name, :l_name, :email, :phone, :password, :user_role_id)");
		// Bind values
		$this->db->bind(":f_name", $data['f_name']);
		$this->db->bind(":l_name", $data['l_name']);
		$this->db->bind(":email", $data['email']);
		$this->db->bind(":phone", $data['phone']);
		$this->db->bind(":password", $data['password']);
		$this->db->bind(":user_role_id", $data['user_role_id']);
		// Execute
		if ($this->db->execute()) {
			// Insert user into drivers table
			$isDriver = $this->getDriverId();
			if($data['user_role_id'] == $isDriver->id){
				$id = $this->db->getLastInsertId();
				$this->db->query("INSERT INTO drivers (user_id) VALUES (:id)");
				$this->db->bind(":id", $id);
				$this->db->execute();
			}
			return true;
		} else {
			return false;
		}
	}

	// Login User
	public function login($email, $password)
	{
		$this->db->query("SELECT * FROM users WHERE email = :email");
		$this->db->bind(":email", $email);
		$row = $this->db->single();

		$hashed_password = $row->password;
		// password_verify php built-in func
		if (password_verify($password, $hashed_password)) {
			return $row;
		} else {
			return false;
		}
	}

	// Get user by Email
	public function getUserByEmail($email)
	{
		$this->db->query('SELECT * FROM users WHERE email = :email');
		// Bind value
		$this->db->bind(":email", $email);
		$row = $this->db->single();
		return $row;
	}

	// Find user by email
	public function findUserByEmail($email)
	{
		$this->db->query('SELECT * FROM users WHERE email = :email');
		// Bind value
		$this->db->bind(":email", $email);
		$row = $this->db->single();

		// Check row
		if ($this->db->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// Find user by phone
	public function findUserByPhone($phone)
	{
		$this->db->query('SELECT * FROM users WHERE phone = :phone');
		$this->db->bind(":phone", $phone);
		$this->db->single();

		if ($this->db->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// Get User By ID
	public function getUserById($id)
	{
		$this->db->query('SELECT * FROM users WHERE id = :id');
		// Bind value
		$this->db->bind(":id", $id);
		$row = $this->db->single();

		return $row;
	}

	public function updateUser($data)
	{
		$this->db->query('UPDATE users SET f_name = :f_name, l_name = :l_name, address = :address WHERE id = :id');
		$this->db->bind(":id", $data['id']);
		$this->db->bind(":f_name", $data['f_name']);
		$this->db->bind(":l_name", $data['l_name']);
		$this->db->bind(":address", $data['address']);

		if ($this->db->execute()) {
			return true;
		} else {
			return false;
		}
	}

	public function getRoles()
	{
		$this->db->query('SELECT * FROM user_roles');
		$roles = $this->db->resultSet();
		return $roles;
	}

	public function checkUserType($id)
	{
		$this->db->query('SELECT * FROM users WHERE id = :id');
		$this->db->bind(':id', $id);
		$row = $this->db->single();
		return $row;
	}

	public function getDriverTansport($id)
	{
		$this->db->query('SELECT * FROM drivers WHERE id = :id');
		$this->db->bind(":id", $id);
		
	}

	public function getDriverId()
	{
		$this->db->query('SELECT id FROM user_roles WHERE id = 5'); // id = 5 == Водитель
		$row = $this->db->single();
		return $row;
	}

	public function getClientId()
	{
		$this->db->query('SELECT id FROM user_roles WHERE id = 6');
		$row = $this->db->single();
		return $row;
	}

	public function checkDriverTransport($id)
	{
		$this->db->query('SELECT transport_id FROM drivers WHERE user_id = :user_id');
		$this->db->bind(":user_id", $id);
		$row = $this->db->single();

		if(!empty($row->transport_id)){
			return true;
		}else{
			return false;
		}
	}

	public function driverTransportType($u_id)
	{
		$this->db->query("SELECT t.t_type_id FROM drivers d JOIN users u ON d.user_id = u.id JOIN transport t ON d.transport_id = t.id WHERE d.user_id = :id");
		$this->db->bind(":id", $u_id);
		return $this->db->single();
	}
}
