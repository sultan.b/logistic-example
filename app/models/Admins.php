<?php
    class Admins 
    {
        private $db;
        
        public function __construct(){
            $this->db = new Database();	
        }

        public function getUsers(){
			$this->db->query("SELECT * FROM users");
			$results = $this->db->resultSet();
			return $results;
        }
        
        public function showUser($userId)
        {
            $this->db->query("SELECT * FROM users WHERE id = :userId");
			$this->db->bind(":userId", $userId);
			$results = $this->db->resultSet();
			return $results;
        }

        public function getDrivers()
        {
            $this->db->query("SELECT * FROM drivers d JOIN users u ON d.user_id = u.id JOIN transport t ON d.transport_id = t.id");
			$results = $this->db->resultSet();
			return $results;
        }

        public function getClientOrders($type_id)
        {
            $query = "SELECT 
            uao.id, uao.driver_id, uao.client_order_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, 
            co.user_id, co.first_address, co.last_address, co.comment, co.price, co.start_date,  
            u.f_name, u.l_name, 
            t.t_name, t.t_state_number, 
            tt.name 
                FROM client_orders co 
                LEFT JOIN user_accepted_orders uao on co.id = uao.client_order_id
                LEFT JOIN drivers d on uao.driver_id = d.user_id
                LEFT JOIN transport t ON d.transport_id = t.id 
                LEFT JOIN users u ON d.user_id = u.id
                JOIN transport_types tt ON co.order_type_id = tt.id 
                WHERE co.order_type_id = :t_type_id";
            $this->db->query($query);
            $this->db->bind(":t_type_id", $type_id);
			$results = $this->db->resultSet();
			return $results;
        }

        public function getClientOrdersById($userId)
        {
            $this->db->query("SELECT * FROM client_orders WHERE user_id = :userId");
            $this->db->bind(":userId", $userId);
			$results = $this->db->resultSet();
			return $results;
        }

        public function getClientOrderByOrderId($data)
        {
            $query = "SELECT uao.id, uao.driver_id, uao.client_order_id, co.user_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, co.first_address, co.last_address, co.comment, co.price, u.f_name, u.l_name, u.phone, t.t_name, t.t_state_number 
                FROM user_accepted_orders uao 
                JOIN client_orders co on uao.client_order_id = co.id 
                JOIN drivers d on uao.driver_id = d.user_id
                JOIN transport t ON d.transport_id = t.id 
                JOIN users u ON d.user_id = u.id 
                where co.order_type_id = 1 and co.user_id = :user_id and uao.id = :acceptedOrderId";
            $this->db->query($query);
            $this->db->bind(":user_id", $data['user_id']);
           // $this->db->bind(":order_id", $data['orderId']);
            //$this->db->bind(":driverId", $data['driverId']);
            $this->db->bind(":acceptedOrderId", $data['acceptedOrderId']);
			$results = $this->db->single();
			return $results;
        }

        public function getOrders() // Тут нужно доработать
        {
            $query = "SELECT 
            uao.id, uao.driver_id, uao.client_order_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, 
            co.user_id, co.first_address, co.last_address, co.comment, co.price, co.start_date,  
            u.f_name, u.l_name, 
            t.t_name, t.t_state_number, 
            tt.name 
                FROM client_orders co 
                LEFT JOIN user_accepted_orders uao on co.id = uao.client_order_id
                LEFT JOIN drivers d on uao.driver_id = d.user_id
                LEFT JOIN transport t ON d.transport_id = t.id 
                LEFT JOIN users u ON d.user_id = u.id
                JOIN transport_types tt ON co.order_type_id = tt.id";
            $this->db->query($query);
			$results = $this->db->resultSet();
			return $results;
        }

        public function getRoles()
        {
            $this->db->query("SELECT * FROM user_roles");
            $results = $this->db->resultSet();
            return $results;
        }

        public function getRoleById($id)
        {
            $this->db->query("SELECT * FROM user_roles WHERE id = :id");
            $this->db->bind(":id", $id);
            $results = $this->db->single();
            return $results;
        }

        public function addRole($data)
        {
            $this->db->query("INSERT INTO user_roles (role_name) VALUES (:role_name)");
			$this->db->bind(":role_name", $data['role_name']);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}
        }

        public function editRole($data)
        {
            $this->db->query('UPDATE user_roles SET role_name = :role_name WHERE id = :id');
            $this->db->bind(":id", $data['id']);
            $this->db->bind(":role_name", $data['role_name']);

            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
            
        }

        public function deleteRoles($id)
        {
            $this->db->query("DELETE FROM user_roles WHERE id = :id");
			$this->db->bind(":id", $id);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}
        }

        public function updateUser($data)
		{
			$this->db->query('UPDATE users SET f_name = :f_name, l_name = :l_name, address = :address WHERE id = :id');
			$this->db->bind(":id", $data['id']);
			$this->db->bind(":f_name", $data['f_name']);
			$this->db->bind(":l_name", $data['l_name']);
			$this->db->bind(":address", $data['address']);
			
			if ($this->db->execute()) {
				return true;
			} else {
				return false;
			}
        }
        
        public function getTransportTypes()
        {
            # code...
            $this->db->query('SELECT * FROM transport_types');
            $ret = $this->db->resultSet();
            return $ret;
        }

        public function getSubTypesById($id)
        {
            # code...
            $this->db->query('SELECT * FROM transport_subtype WHERE transport_type_id = :id');
            $this->db->bind(":id", $id);
            $subtypes = $this->db->resultSet();

            return $subtypes;
        }

        public function getTransportTypeById($id)
        {
            # code...
            $this->db->query('SELECT * FROM transport_types WHERE id = :id');
            $this->db->bind(":id", $id);
            $row = $this->db->single();

            return $row;
        }

        public function addSubType($data)
        {
            $this->db->query('INSERT INTO transport_subtype (transport_name, transport_type_id) VALUE (:transport_name, :transport_type_id)');
            $this->db->bind(":transport_name", $data['transport_name']);
            $this->db->bind(":transport_type_id", $data['transport_type_id']);

            if($this->db->execute()){
				return true;
			} else {
				return false;
			}
        }

        public function deleteSubType($id)
        {
            $this->db->query("DELETE FROM transport_subtype WHERE id = :id");
			$this->db->bind(":id", $id);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}
        }

        public function checkDriverTransportId($user_id)
        {
            $this->db->query('SELECT transport_id FROM drivers WHERE user_id = :user_id');
            $this->db->bind(":user_id", $user_id);
            $row = $this->db->single();
            return $row;
        }

        public function addTransport($data)
        {
            $now = date('Y-m-d H:i:s');
            $this->db->query('INSERT INTO transport (t_name, t_type_id, t_state_number, subtype_id, user_id, created_at, updated_at) VALUE (:t_name, :t_type_id, :t_state_number, :subtype_id, :user_id, :created_at, :updated_at)');
            $this->db->bind(":t_name", $data['t_name']);
            $this->db->bind(":t_type_id", $data['t_type_id']);
            $this->db->bind(":t_state_number", $data['t_state_number']);
            $this->db->bind(":subtype_id", $data['subtype_id']);
            $this->db->bind(":user_id", $data['user_id']);
            $this->db->bind(":created_at", $now);
            $this->db->bind(":updated_at", $now);

            if($this->db->execute()){
                $id = $this->db->getLastInsertId();
                $check = $this->checkDriverTransportId($data['user_id']);
                if($check->transport_id == null){
                    $this->db->query('UPDATE drivers SET transport_id = :t_id WHERE user_id = :user_id');
                    $this->db->bind(":t_id", $id);
                    $this->db->bind(":user_id", $data['user_id']);
                    $this->db->execute();
                }
				return true;
			} else {
				return false;
			}
        }

        public function getDriverTransport($user_id)
        {
            $this->db->query('select d.user_id, t.id, t.t_name, t.t_state_number, t.subtype_id, t.created_at, t.updated_at from drivers d inner join transport t on d.transport_id = t.id
                                where d.user_id = :user_id');
            $this->db->bind(":user_id", $user_id);
            $row = $this->db->single();

            return $row;
        }

        public function editTransport($data)
        {
            $now = date('Y-m-d H:i:s');
            $this->db->query('UPDATE transport SET t_name = :t_name, t_state_number = :t_state_number, updated_at = :updated_at WHERE user_id = :user_id');
            $this->db->bind(":t_name", $data['t_name']);
            $this->db->bind(":t_state_number", $data['t_state_number']);
            $this->db->bind(":user_id", $data['user_id']);
            $this->db->bind(":updated_at", $now);

            if($this->db->execute()){
				return true;
			} else {
				return false;
			}
        }

        public function deleteTransport($id)
        {
            $this->db->query('DELETE FROM transport WHERE id = :id');
            $this->db->bind(":id", $id);

            if($this->db->execute()){
                $this->db->query('UPDATE drivers SET transport_id = NULL WHERE transport_id = :transport_id');
                $this->db->bind(":transport_id", $id);
                $this->db->execute();
                return true;
            }else{
                return false;
            }
        }

        public function addClientOrder($data)
        {
            $now = date('Y-m-d H:i:s');
            $query = 'INSERT INTO 
                client_orders(
                    first_address, 
                    last_address, 
                    price, 
                    comment, 
                    start_date, 
                    order_type_id, 
                    user_id, 
                    is_accepted, 
                    created_at, 
                    updated_at
                ) 
                VALUE(
                    :first_address, 
                    :last_address, 
                    :price, 
                    :comment, 
                    :start_date, 
                    :order_type_id, 
                    :user_id, 
                    :is_accepted, 
                    :created_at, 
                    :updated_at
                    )';
            $this->db->query($query);
            $this->db->bind(':first_address', $data['first_address']);
            $this->db->bind(':last_address', $data['last_address']);
            $this->db->bind(':price', $data['price']);
            $this->db->bind(':comment', $data['comment']);
            $this->db->bind(':start_date', $data['start_date']);
            $this->db->bind(':order_type_id', $data['order_type_id']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':is_accepted', $data['is_accepted']);
            $this->db->bind(':created_at', $now);
            $this->db->bind(':updated_at', $now);

            if($this->db->execute()){
                $id = $this->db->getLastInsertId();
                return $id;
			} else {
				return null;
			}
        }

        public function addOrderToAccep($order_id, $user_id)
        {
            $this->db->query('INSERT INTO user_accepted_orders(client_order_id, driver_id, is_accepted) VALUES(:order_id, :user_id, :accepted)');
            $this->db->bind(":order_id", $order_id);
            $this->db->bind(":user_id", $user_id);
            $this->db->bind(":accepted", 1);

            if($this->db->execute()){
                $this->db->query('UPDATE client_orders SET is_accepted = 1 where id = :id');
                $this->db->bind(":id", $order_id);
                $this->db->execute();
                return true;
            }else{
                return false;
            }
        }

        public function getAcceptedOrderByUserId($user_id, $order_id)
        {
            $query = "SELECT uao.id, uao.driver_id, uao.client_order_id, co.user_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, co.first_address, co.last_address, co.comment, u.f_name, u.l_name, t.t_name, t.t_state_number 
                FROM user_accepted_orders uao 
                JOIN client_orders co on uao.client_order_id = co.id 
                JOIN drivers d on uao.driver_id = d.user_id
                JOIN transport t ON d.transport_id = t.id 
                JOIN users u ON d.user_id = u.id 
                where co.user_id = :user_id and uao.client_order_id = :order_id";
            $this->db->query($query);
            $this->db->bind(":order_id", $order_id);
            $this->db->bind(":user_id", $user_id);

            $result = $this->db->single();
            return $result;
        }

        public function getTaxiOrdersByClientId($id)
        {
            $query = "SELECT 
            uao.id, uao.driver_id, uao.client_order_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, 
            co.user_id, co.first_address, co.last_address, co.comment, co.price, co.start_date,  
            u.f_name, u.l_name, 
            t.t_name, t.t_state_number, 
            tt.name 
                FROM client_orders co 
                LEFT JOIN user_accepted_orders uao on co.id = uao.client_order_id
                LEFT JOIN drivers d on uao.driver_id = d.user_id
                LEFT JOIN transport t ON d.transport_id = t.id 
                LEFT JOIN users u ON d.user_id = u.id
                JOIN transport_types tt ON co.order_type_id = tt.id
                where co.order_type_id = 1 and co.user_id = :user_id";
            $this->db->query($query);
            $this->db->bind(":user_id", $id);

            $ret = $this->db->resultSet();

            return $ret;
        }

        public function getFreightOrdersByClientId($id)
        {
            $query = "SELECT 
            uao.id, uao.driver_id, uao.client_order_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, 
            co.user_id, co.first_address, co.last_address, co.comment, co.price, co.start_date,  
            u.f_name, u.l_name, 
            t.t_name, t.t_state_number, 
            tt.name 
                FROM client_orders co 
                LEFT JOIN user_accepted_orders uao on co.id = uao.client_order_id
                LEFT JOIN drivers d on uao.driver_id = d.user_id
                LEFT JOIN transport t ON d.transport_id = t.id 
                LEFT JOIN users u ON d.user_id = u.id
                JOIN transport_types tt ON co.order_type_id = tt.id
                where co.order_type_id = 2 and co.user_id = :user_id";
            $this->db->query($query);
            $this->db->bind(":user_id", $id);

            $ret = $this->db->resultSet();

            return $ret;
        }

        public function getSubtypesByTypeId($typeId)
        {
            $this->db->query('SELECT * FROM transport_subtype WHERE transport_type_id = :typeId');
            $this->db->bind("typeId", $typeId);
            $result = $this->db->resultSet();

            return $result;
        }

        public function addOtherOrder($data)
        {
            $now = date('Y-m-d H:i:s');
            $query = 'INSERT INTO 
            client_orders(
                work_title, 
                first_address, 
                comment, 
                price, 
                order_type_id, 
                order_subtype_id, 
                start_date, 
                user_id, 
                is_accepted, 
                created_at, 
                updated_at
                ) 
            VALUE(
                :work_title, 
                :first_address, 
                :comment, 
                :price, 
                :order_type_id,
                :order_subtype_id, 
                :date_start, 
                :user_id, 
                :is_accepted, 
                :created_at, 
                :updated_at)';
            $this->db->query($query);
            $this->db->bind(':work_title', $data['work_title']);
            $this->db->bind(':first_address', $data['first_address']);
            $this->db->bind(':comment', $data['comment']);
            $this->db->bind(':price', $data['price']);
            $this->db->bind(':date_start', $data['date_start']);
            $this->db->bind(':order_type_id', $data['order_type_id']);
            $this->db->bind(':order_subtype_id', $data['order_subtype_id']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':is_accepted', $data['is_accepted']);
            $this->db->bind(':created_at', $now);
            $this->db->bind(':updated_at', $now);

            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function getOtherOrdersByClientId($id)
        {
            $query = "SELECT 
            uao.id, uao.driver_id, uao.client_order_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, 
            co.user_id, co.first_address, co.work_title, co.comment, co.price, co.start_date,  
            u.f_name, u.l_name, 
            t.t_name, t.t_state_number, 
            ts.transport_name 
                FROM client_orders co 
                LEFT JOIN user_accepted_orders uao on co.id = uao.client_order_id
                LEFT JOIN drivers d on uao.driver_id = d.user_id
                LEFT JOIN transport t ON d.transport_id = t.id 
                LEFT JOIN users u ON d.user_id = u.id
                JOIN transport_types tt ON co.order_type_id = tt.id 
                JOIN transport_subtype ts ON co.order_subtype_id = ts.id
                where co.order_type_id = 3 and co.user_id = :user_id";
            $this->db->query($query);
            $this->db->bind(":user_id", $id);

            $ret = $this->db->resultSet();

            return $ret;
        }

        public function getBusOrdersByClientId($id)
        {
            $query = "SELECT 
            uao.id, uao.driver_id, uao.client_order_id, uao.is_accepted, uao.is_started, uao.is_finished, uao.is_waited, 
            co.user_id, co.first_address, co.last_address, co.comment, co.price, co.start_date,  
            u.f_name, u.l_name, 
            t.t_name, t.t_state_number, 
            ts.transport_name 
                FROM client_orders co 
                LEFT JOIN user_accepted_orders uao on co.id = uao.client_order_id
                LEFT JOIN drivers d on uao.driver_id = d.user_id
                LEFT JOIN transport t ON d.transport_id = t.id 
                LEFT JOIN users u ON d.user_id = u.id
                JOIN transport_types tt ON co.order_type_id = tt.id 
                JOIN transport_subtype ts ON co.order_subtype_id = ts.id
                where co.order_type_id = 4 and co.user_id = :user_id";
            $this->db->query($query);
            $this->db->bind(":user_id", $id);

            $ret = $this->db->resultSet();

            return $ret;
        }

        public function addBusOrder($data)
        {
            $now = date('Y-m-d H:i:s');
            $query = 'INSERT INTO 
            client_orders(
                first_address, 
                last_address, 
                comment, 
                price, 
                order_type_id, 
                order_subtype_id, 
                start_date, 
                user_id, 
                is_accepted, 
                created_at, 
                updated_at
                ) 
            VALUE(
                :first_address, 
                :last_address, 
                :comment, 
                :price, 
                :order_type_id,
                :order_subtype_id, 
                :date_start, 
                :user_id, 
                :is_accepted, 
                :created_at, 
                :updated_at)';
            $this->db->query($query);
            $this->db->bind(':first_address', $data['first_address']);
            $this->db->bind(':last_address', $data['last_address']);
            $this->db->bind(':comment', $data['comment']);
            $this->db->bind(':price', $data['price']);
            $this->db->bind(':date_start', $data['start_date']);
            $this->db->bind(':order_type_id', $data['order_type_id']);
            $this->db->bind(':order_subtype_id', $data['order_subtype_id']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':is_accepted', $data['is_accepted']);
            $this->db->bind(':created_at', $now);
            $this->db->bind(':updated_at', $now);

            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function getFaq()
        {
            $this->db->query('SELECT * FROM faq');
            return $this->db->resultSet();
        }

        public function addFaq($data)
        {
            $now = date('Y-m-d H:i:s');
            $query = "INSERT INTO faq(question, answer, for_client, created_at, updated_at) VALUES(:question, :answer, :for_client, :created_at, :updated_at)";
            $this->db->query($query);
            $this->db->bind(":question", $data['question']);
            $this->db->bind(":answer", $data['answer']);
            $this->db->bind(":for_client", $data['for_client']);
            $this->db->bind(":created_at", $now);
            $this->db->bind(":updated_at", $now);

            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function UpdateFaQ($data)
        {
            $now = date('Y-m-d H:i:s');
            $this->db->query('UPDATE faq SET question = :question, answer = :answer, for_client = :for_client, updated_at = :updated_at WHERE id = :id');
            $this->db->bind(":question", $data['question']);
            $this->db->bind(":answer", $data['answer']);
            $this->db->bind(":for_client", $data['for_client']);
            $this->db->bind(":id", $data['id']);
            $this->db->bind(":updated_at", $now);

            if($this->db->execute()){
				return true;
			} else {
				return false;
			}
        }

        public function RemoveFaq($id)
        {
            $this->db->query("DELETE FROM faq WHERE id = :id");
			$this->db->bind(":id", $id);
			if($this->db->execute()){
				return true;
			} else {
				return false;
			}
        }

        public function getFaqById($id)
        {
            $this->db->query('SELECT * FROM faq WHERE id = :id');
            $this->db->bind(":id", $id);
            return $this->db->single();
        }

        public function getClientFaq()
        {
            $this->db->query('SELECT * FROM faq WHERE for_client = 1');
            return $this->db->resultSet();
        }

        public function getDriverFaq()
        {
            $this->db->query('SELECT * FROM `faq` WHERE (for_client IS NULL or for_client = 0)');
            return $this->db->resultSet();
        }
    }
