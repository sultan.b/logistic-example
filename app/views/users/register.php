<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container my-5 pb-5">
	<div class="row">
		<div class="col-md-6 mx-auto">
			<div class="card card-body border-secondary bg-light mt-5 mb-5">
				<h2 class="text-dark">Регистрация</h2>
				<form action="<?php echo URLROOT ?>/users/register" method="POST">
					<div class="form-group">
						<label for="f_name">Имя: <sup>*</sup></label>
						<input type="text" name="f_name" class="form-control <?php echo (!empty($data['f_name_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['f_name'] ?>">
						<span class="invalid-feedback"><?php echo $data['f_name_err'] ?></span>
					</div>
					<div class="form-group">
						<label for="l_name">Фамилия: <sup>*</sup></label>
						<input type="text" name="l_name" class="form-control <?php echo (!empty($data['l_name_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['l_name'] ?>">
						<span class="invalid-feedback"><?php echo $data['l_name_err'] ?></span>
					</div>
					<div class="form-group">
						<label for="email">Email: <sup>*</sup></label>
						<input type="email" name="email" class="form-control <?php echo (!empty($data['email_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['email'] ?>">
						<span class="invalid-feedback"><?php echo $data['email_err'] ?></span>
					</div>
					<div class="form-group">
						<label for="phone">Телефон: <sup>*</sup></label>
						<input type="text" id="phone" name="phone" placeholder="+7 (000) 000-0000" class="form-control <?php echo (!empty($data['phone_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['phone'] ?>">
						<span class="invalid-feedback"><?php echo $data['phone_err'] ?></span>
					</div>
					<div class="form-group">
						<label for="password">Пароль: <sup>*</sup></label>
						<input type="password" name="password" class="form-control <?php echo (!empty($data['password_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['password'] ?>">
						<span class="invalid-feedback"><?php echo $data['password_err'] ?></span>
					</div>
					<div class="form-group">
						<label for="confirm_password">Подтверите пароль: <sup>*</sup></label>
						<input type="password" name="confirm_password" class="form-control <?php echo (!empty($data['confirm_password_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['confirm_password'] ?>">
						<span class="invalid-feedback"><?php echo $data['confirm_password_err'] ?></span>
					</div>
					<div class="form-group">
						<label for="user-role">Выберите роль: <sup>*</sup></label>
						<select class="form-control" id="user-selector">
							<option>Выберите роль</option>
							<?php
							foreach ($data['user_roles'] as $roles) { ?>
								<option value="<?php echo $roles->id; ?>"><?php echo $roles->role_name; ?></option>
							<?php
							}
							?>
						</select>
						<input type="hidden" name="user_role_id" id="user_role_id" value="" />
					</div>
					<div class="row">
						<div class="col">
							<input type="submit" value="Регистрация" class="btn btn-success btn-block">
						</div>
						<div class="col">
							<a href="<?php echo URLROOT; ?>/users/login" class="btn btn-light btn-block">Вы уже зарегистрированы? Логин</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>
<script>
	$('#phone').inputmask("mask", {
		"mask": "+7 (999) 999-9999"
	});

	$("#user-selector").on('change', function() {
		$('#user_role_id').val($(this).find(":selected").val());
	});
</script>