<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="card w-100">
                <div class="card-body">
                    <h5 class="card-title">Добавить подтип</h5>
                    <form action="<?php echo URLROOT ?>/admin/addsubtypes/<?php echo $data['typeId'] ?>" method="POST">
                    <input type="hidden" name="transport_type_id" value="<?php echo $data['typeId']; ?>"/>
                        <div class="form-group">
                            <label for="transport_name">Название: </label>
                            <input type="text" name="transport_name" class="form-control<?php echo (!empty($data['transport_name_err'])) ? ' is-invalid' : '' ?>" value="<?php echo $data['transport_name'] ?>">
                            <span class="invalid-feedback"><?php echo $data['transport_name_err'] ?></span>
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="submit" value="Добавить" class="btn btn-success btn-block">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>