<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php
        if ($_SESSION['admin_mode']) {
            if (!empty($data['drivers'])) { ?>
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Фамилия</th>
                            <th scope="col">Телефон</th>
                            <th scope="col">Адрес</th>
                            <th scope="col">Транспорт</th>
                            <th scope="col">Гос номер</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1;

                        foreach ($data['drivers'] as $items) { ?>
                            <tr>
                                <th scope="row"><?php echo $i ?></th>
                                <td><?php echo $items->f_name; ?></td>
                                <td><?php echo $items->l_name; ?></td>
                                <td><?php echo $items->phone; ?></td>
                                <td><?php echo $items->address; ?></td>
                                <td><?php echo $items->t_name; ?></td>
                                <td><?php echo $items->t_state_number; ?></td>
                            </tr>
                        <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
        <?php
            } else {
                echo '<div class="alert alert-danger w-100 text-center">Водителей не найдено</div>';
            }
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>