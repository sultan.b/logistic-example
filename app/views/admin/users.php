<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Фамилия</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Адрес</th>
                        <th scope="col">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;
                    foreach ($data['users'] as $items) { ?>
                        <tr>
                            <th scope="row"><?php echo $i ?></th>
                            <td><?php echo $items->f_name; ?></td>
                            <td><?php echo $items->l_name; ?></td>
                            <td><?php echo $items->phone; ?></td>
                            <td><?php echo $items->address; ?></td>
                            <td><a class="btn btn-primary" href="/admin/edituser/<?php echo $items->id; ?>">Редактировать</a></td>
                        </tr>
                    <?php $i++;
                    } ?>
                </tbody>
            </table>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>