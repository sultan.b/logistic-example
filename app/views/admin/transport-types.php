<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <h5>Типы</h5>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Тип</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;
                    foreach ($data['t_types'] as $items) { ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td>
                                <?php if ($items->name === "Легковые" || $items->name === "Грузоперевозки") :
                                    echo $items->name;
                                else : 
                                ?>
                                    <a href="/admin/subtypes/<?php echo $items->id; ?>"><?php echo $items->name; ?> </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php $i++;
                    } ?>
                </tbody>
            </table>

        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>