<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="card w-100">
                <div class="card-body">
                    <h5 class="card-title">Типы</h5>
                    <a href="/admin/addsubtypes/<?php echo $data['type']->id; ?>" class="btn btn-primary mb-3">Добавить</a>
                    <?php if ($data['subtypes'] != null) : ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col"><?php echo $data['type']->name; ?></th>
                                    <th scope="col">Действие</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($data['subtypes'] as $items) { ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td>
                                            <?php echo $items->transport_name; ?>
                                        </td>
                                        <td>
                                            <a sub-id="<?php echo $items->id; ?>" class="btn btn-primary m-1" href="#" data-toggle="modal" data-target="#deleteSubTypeModal">Удалить</a>
                                        </td>
                                    </tr>
                                <?php $i++;
                                } ?>
                            </tbody>
                        </table>
                    <?php else : ?>
                        <div class="alert alert-warning w-100 text-center">Подтипов не найдено</div>
                    <?php endif; ?>
                </div>
            </div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>
<?php if (isset($_SESSION['admin_mode'])) : ?>
    <!-- Modal -->
    <div class="modal fade" id="deleteSubTypeModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Удалить подтип</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Вы точно хотите удалить подтип?
                </div>
                <div class="modal-footer">
                    <form class="pull-right" action="" method="POST">
                    <input type="hidden" id="subTypeId" value=""/>
                        <input type="submit" id="deleteSubTypeBtn" value="Удалить" class="btn btn-danger">
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>

    $('#deleteSubTypeModal').on('show.bs.modal', function (e) {
        var opener = e.relatedTarget;
        var id = $(opener).attr('sub-id');
        $("#subTypeId").val(id);
    });
    $("#deleteSubTypeBtn").click(function(e) {
        var id = $('#subTypeId').val();
        $.ajax({
            url: "<?php echo URLROOT ?>/admin/deleteSubType/" + id,
            type: "post",
            success: function(response) {
                console.log(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });

    });
</script>