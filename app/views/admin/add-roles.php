<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container p-3">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="card" style="width: 100%;">
                <div class="card-body">
                    <h5 class="card-title">Добавить роли</h5>
                    <div class="card-text">
                        <form action="<?php echo URLROOT ?>/admin/addroles" method="POST">
                            <div class="form-group">
                                <label for="role_name">Роль: </label>
                                <input type="text" name="role_name" class="form-control<?php echo (!empty($data['role_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['role_name'] ?>">
                                <span class="invalid-feedback"><?php echo $data['role_err'] ?></span>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <input type="submit" value="Добавить" class="btn btn-success btn-block">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>