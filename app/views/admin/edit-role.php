<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="col-md-6 mx-auto">
                <div class="card card-body mt-5 mb-5">
                    <h3 class="text-dark">Редактирование</h2>
                        <form action="<?php echo URLROOT ?>/admin/editrole/<?php echo $data['id']; ?>" method="POST">
                            <div class="form-group">
                                <label for="role_name">Роль: </label>
                                <input type="text" name="role_name" class="form-control<?php echo (!empty($data['role_err'])) ? 'is-invalid' : '' ?>" value="<?php echo $data['role_name'] ?>">
                                <span class="invalid-feedback"><?php echo $data['role_err'] ?></span>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <input type="submit" value="Сохранить" class="btn btn-success btn-block">
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>