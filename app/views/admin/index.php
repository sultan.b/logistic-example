<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <?php if ($_SESSION['admin_mode']) { ?>
        <div class="row">
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center">Пользователи</h5>
                        <p class="card-text text-center card-count-text">100</p>
                        <a href="/admin/users" class="card-link stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center">Водители</h5>
                        <p class="card-text text-center card-count-text">32</p>
                        <a href="/admin/drivers" class="card-link stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center">Роли</h5>
                        <p class="card-text text-center card-count-text">3</p>
                        <a href="/admin/roles" class="card-link stretched-link"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-3">
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center">Заказы</h5>
                        <p class="card-text text-center card-count-text">100</p>
                        <a href="/admin/orders" class="card-link stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center">Вопрос-ответы</h5>
                        <p class="card-text text-center card-count-text">10</p>
                        <a href="/admin/faq" class="card-link stretched-link"></a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center">Типы транспортов</h5>
                        <p class="card-text text-center card-count-text">4</p>
                        <a href="/admin/transporttypes" class="card-link stretched-link"></a>
                    </div>
                </div>
            </div>
        </div>

    <?php
    } else {
        echo '<div class="alert alert-danger">У вас нет доступа</div>';
    } ?>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>