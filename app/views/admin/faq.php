<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="card w-100">
                <div class="card-body">
                    <h4 class="card-title">Вопрос-ответы</h4>
                    <h6 class="card-subtitle mb-2 text-muted">
                        <a class="btn btn-success" href="#" data-toggle="modal" data-target="#addModal">Добавить</a>
                    </h6>
                    <div class="card-text">
                        <?php if ($data['faq'] != null) : ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Вопрос</th>
                                        <th scope="col">Ответ</th>
                                        <th scope="col">Действие</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($data['faq'] as $items) : ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $items->question ?></td>
                                            <td><?php echo $items->answer ?></td>
                                            <td>
                                                <a class="btn btn-primary m-1" href="#" faqs-id="<?php echo $items->id; ?>" data-toggle="modal" data-target="#editModal">Редактировать</a>
                                                <a class="btn btn-primary m-1" href="#" faq-id="<?php echo $items->id; ?>" data-toggle="modal" data-target="#deleteModal">Удалить</a>
                                            </td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <div class="alert alert-info">Вопрос-ответ пусто</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>
<?php if (isset($_SESSION['admin_mode'])) : ?>
    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить вопрос-ответ</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="addFaqForm">
                        <div class="form-group">
                            <label for="question">Вопрос: </label>
                            <input type="text" name="question" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="answer">Ответ: </label>
                            <textarea name="answer" id="" rows="3" class="form-control"></textarea>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" id="forClientCheckBox" name="for_client" class="form-check-input" id="forClient">
                            <label class="form-check-label" for="forClient">Для клиентов</label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btnAddFaq">Добавить</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить вопрос-ответ</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="EditFaqForm">
                        <input type="hidden" name="id" id="faqIds" value="" />
                        <div class="form-group">
                            <label for="question">Вопрос: </label>
                            <input type="text" name="question" id="editQuestion" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="answer">Ответ: </label>
                            <textarea name="answer" id="editAnswer" rows="3" class="form-control"></textarea>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" id="editForClientCheckBox" name="for_client" class="form-check-input" id="forClient">
                            <label class="form-check-label" for="forClient">Для клиентов</label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btnEditFaq">Сохранить</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить вопрос-ответ</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Вы точно хотите удалить вопрос-ответ?
                </div>
                <div class="modal-footer">
                    <form class="pull-right" action="" method="POST">
                        <input type="hidden" name="id" id="faqId" value="" />
                        <input type="submit" id="deleteFaqBtn" value="Удалить" class="btn btn-danger">
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>
    $("#btnAddFaq").click(function() {
        $.ajax({
            url: "<?php echo URLROOT ?>/admin/addFaq/",
            type: "post",
            data: $("#addFaqForm").serialize(),
            success: function(response) {
                console.log("resopnce =" + response);
                if(response == 1){
                    window.location = "<?php echo URLROOT ?>/admin/faq";
                }else{
                    alert("Ошибка добавления");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
    });

    $("#forClientCheckBox").change(function() {
        if (this.checked) {
            $(this).val(1);
        } else {
            $(this).val("");
        }
    });

    $('#deleteModal').on('show.bs.modal', function (e) {
        var opener = e.relatedTarget;
        var id = $(opener).attr('faq-id');
        $("#faqId").val(id);
    });

    $("#deleteFaqBtn").click(function() {
        var id = $("#faqId").val();
        $.ajax({
            url: "<?php echo URLROOT ?>/admin/deleteFaq/" + id,
            type: "post",
            success: function(response) {
                console.log("resopnce = " + response);
                var data = JSON.parse(response);
                console.log("data = " + data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
    });

    $('#editModal').on('show.bs.modal', function (e) {
        var opener = e.relatedTarget;
        var id = $(opener).attr('faqs-id');
        console.log(id);
        $.ajax({
            url: "<?php echo URLROOT ?>/admin/getFaqById/" + id,
            type: "post",
            success: function(response) {
                console.log("resopnce = " + response);
                var data = JSON.parse(response);
                $("#faqIds").val(data.faq.id);
                $("#editQuestion").val(data.faq.question);
                $("#editAnswer").val(data.faq.answer);
                if(data.faq.for_client != null){
                    $('#editForClientCheckBox').prop('checked', true);
                    $("#editForClientCheckBox").val(1);
                }else{
                    $("#editForClientCheckBox").val("");
                }
                $("#editQuestion").val(data.faq.question);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
    });

    $("#btnEditFaq").click(function() {
        var id = $("#faqId").val();
        $.ajax({
            url: "<?php echo URLROOT ?>/admin/updateFaq/",
            type: "post",
            data: $("#EditFaqForm").serialize(),
            success: function(response) {
                if(response == 1){
                    window.location = "<?php echo URLROOT ?>/admin/faq";
                }else{
                    alert("Ошибка редактирования");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
    });
</script>