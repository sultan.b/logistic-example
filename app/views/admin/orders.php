<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="card w-100">
                <div class="card-body">
                    <h5 class="card-title">Заказы</h5>
                    <div class="card-text">
                        <?php if ($data['orders'] != null) : ?>
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Водитель</th>
                                        <th scope="col">Транспорт</th>
                                        <th scope="col">Гос номер</th>
                                        <th scope="col">Откуда</th>
                                        <th scope="col">Куда</th>
                                        <th scope="col">Описание</th>
                                        <th scope="col">Статус</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data['orders'] as $items) : ?>
                                        <tr>
                                            <td><?php echo $items->f_name . " " . $items->l_name; ?></th>
                                            <td><?php echo $items->t_name; ?> </td>
                                            <td><?php echo $items->t_state_number; ?> </td>
                                            <td><?php echo $items->first_address; ?> </td>
                                            <td><?php echo $items->last_address; ?></td>
                                            <td><?php echo $items->comment; ?></td>
                                            <td>
                                                <?php if ($items->is_finished == 1) : ?>
                                                    <p class="text-success">Выполено</p>
                                                <?php elseif ($items->is_accepted != 1) : ?>
                                                    <p class="text-danger">Заказ еще не приянто</p>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <div class="alert alert-info">У вас еще нет заказов</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>