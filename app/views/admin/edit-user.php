<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="col-md-6 mx-auto">
			<div class="card card-body mt-5 mb-5">
				<h3 class="text-dark">Редактирование</h2>
				<form action="<?php echo URLROOT ?>/admin/edituser/<?php echo $data['id']; ?>" method="POST">
					<div class="form-group">
						<label for="f_name">Имя:</label>
						<input type="text" name="f_name" class="form-control" value="<?php echo $data['f_name'] ?>">
					</div>
					<div class="form-group">
						<label for="l_name">Фамилия: </label>
						<input type="text" name="l_name" class="form-control" value="<?php echo $data['l_name'] ?>">
					</div>
					
					<div class="form-group">
						<label for="address">Адрес: </label>
						<input type="text" name="address" class="form-control" value="<?php echo $data['address'] ?>">
					</div>
					<div class="row">
						<div class="col">
							<input type="submit" value="Сохранить" class="btn btn-success btn-block">
						</div>
					</div>
				</form>
			</div>
		</div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>