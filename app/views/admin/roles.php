<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container p-3">
    <div class="row">
        <?php if ($_SESSION['admin_mode']) { ?>
            <div class="card" style="width: 100%;">
                <div class="card-body">
                    <h5 class="card-title">Роли</h5>
                    <!-- <h6 class="card-subtitle mb-2 text-muted">
                        <a class="btn btn-success" href="/admin/addroles">Добавить</a>
                    </h6> -->
                    <div class="card-text">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Роль</th>
                                    <th scope="col">Действие</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($data['roles'] as $items) { ?>
                                    <tr>
                                        <td><?php echo $items->role_name ?></td>
                                        <td>
                                            <input type="hidden" id="delteRoleId" value="<?php echo $items->id; ?>" />
                                            <a class="btn btn-primary m-1" href="/admin/editrole/<?php echo $items->id ?>">Редактировать</a>
                                            <!-- <a class="btn btn-primary m-1" href="#" data-toggle="modal" data-target="#deleteModal">Удалить</a> -->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php
        } else {
            echo '<div class="alert alert-danger w-100 text-center">У вас нет доступа</div>';
        } ?>
    </div>
</div>
<?php if (isset($_SESSION['admin_mode'])) : ?>
    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Удалить роль</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Вы точно хотите удалить роль?
                </div>
                <div class="modal-footer">
                    <form class="pull-right" action="" method="POST">
                        <input type="submit" id="deleteRoleBtn" value="Удалить" class="btn btn-danger">
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>
    $("#deleteRoleBtn").click(function() {
        var id = $('#delteRoleId').val();
        $.ajax({
            url: "<?php echo URLROOT ?>/admin/deleteroles/" + id,
            type: "post",
            success: function(response) {
                console.log("Успешно удалено");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });

    });
</script>