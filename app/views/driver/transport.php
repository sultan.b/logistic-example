<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <?php if ($_SESSION['driver']) : ?>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            Транспорт
                        </h5>
                        <?php if ($data['isAdded'] != 1) : ?>
                            <div class="alert alert-warning">
                                Вы еще не добавили транспорт.<br />
                                <a href="/driver/addtransport" class="btn btn-primary">Добавить</a>
                            </div>
                        <?php else : ?>
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Название</th>
                                        <th scope="col">Гос номер</th>
                                        <th scope="col">Действие</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row"><?php echo 1 ?></th>
                                        <td><?php echo $data['transport']->t_name; ?></td>
                                        <td><?php echo $data['transport']->t_state_number; ?></td>
                                        <td>
                                            <input type="hidden" name="transport_id" id="transport_id" value="<?php echo $data['transport']->id ?>" />
                                            <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#editTransportModal">Редактировать</a>
                                            <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#deleteTransportModal">Удалить</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            <?php else : ?>

            <?php endif; ?>
        </div>
    </div>
</div>

<?php if (isset($_SESSION['driver'])) : ?>
    <!-- Modal Edit -->
    <div class="modal fade" id="editTransportModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Редактировать транспорт</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?php echo URLROOT ?>/driver/edittransport" method="POST">
                        <div class="form-group">
                            <label for="t_name">Название(Марка, модель): <sup>*</sup></label>
                            <input type="text" name="t_name" class="form-control" value="<?php echo $data['transport']->t_name; ?>">
                        </div>
                        <div class="form-group">
                            <label for="t_state_number">Гос. номер: <sup>*</sup></label>
                            <input type="text" name="t_state_number" class="form-control" value="<?php echo $data['transport']->t_state_number; ?>">
                        </div>
                        <div class="row">
                            <div class="col">
                                <input type="submit" value="Сохранить" class="btn btn-success btn-block">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="deleteTransportModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Удалить транспорт</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Вы точно хотите удалить транспорт?
                </div>
                <div class="modal-footer">
                    <form class="pull-right" action="" method="POST">
                        <input type="submit" id="deleteTransportBtn" value="Удалить" class="btn btn-danger">
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>
    $("#deleteTransportBtn").click(function() {
        var id = $('#transport_id').val();
        $.ajax({
            url: "<?php echo URLROOT ?>/driver/deletetransport/" + id,
            type: "post",
            success: function(response) {
                console.log("Успешно удалено");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
    });
</script>