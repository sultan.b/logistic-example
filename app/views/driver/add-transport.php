<?php require APPROOT . '/views/inc/header.php'; ?>
<?php if (isLoggedIn()) : ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php if ($_SESSION['driver'] == true) : ?>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Добавить транспорт</h5>
                            <form action="<?php echo URLROOT ?>/driver/addtransport" method="POST">
                                <div class="form-group">
                                    <label for="t_name">Название(Марка, модель): <sup>*</sup></label>
                                    <input type="text" name="t_name" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="t_state_number">Гос. номер: <sup>*</sup></label>
                                    <input type="text" name="t_state_number" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="t_type">Выберите тип транспорта: <sup>*</sup></label>
                                    <select class="form-control" id="t-type-selector">
                                        <option>Выберите</option>
                                        <?php
                                        foreach ($data['types'] as $items) { ?>
                                            <option value="<?php echo $items->id; ?>"><?php echo $items->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <input type="hidden" name="t_type_id" id="selected-type-id" value="" />
                                </div>
                                <div class="form-group" id="subtype-form" style="display: none;">
                                    <label for="t_type">Выберите подтип: <sup>*</sup></label>
                                    <select class="form-control" id="subtype-selector">
                                        <option>Выберите</option>
                                    </select>
                                    <input type="hidden" id="subtype_id" name="subtype_id" value="" />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <input type="submit" value="Добавить" class="btn btn-success btn-block">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="alert alert-warning">У вас нет доступа</div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>
    $("#t-type-selector").on('change', function() {
        var id = $(this).find(":selected").val();
        $("#selected-type-id").val(id);
        var name = $(this).find(":selected").text();
        if(name === 'Легковые' || name === 'Грузоперевозки'){
            $("#subtype-form").css("display", "none");
        }else{
            $.ajax({
            url: "<?php echo URLROOT ?>/admin/getSubTypesById/" + id,
            success: function(response) {
                var data = JSON.parse(response);
                var output = [];
                output.push('<option>Выберите</option>');
                $.each(data, function(key, value) {
                    output.push('<option value="' + value.id + '">' + value.transport_name + '</option>');
                });

                $('#subtype-selector').html(output.join(''));
                $("#subtype-form").css("display", "block");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
        }
    });
    $("#subtype-form").on('change', function(){
        var id = $(this).find(":selected").val();
        $("#subtype_id").val(id);
    });
</script>