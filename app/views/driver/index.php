<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <h5>Заказы</h5>
        <?php foreach ($data['orders'] as $items) : ?>
            <div class="card w-100 mb-2 ">
                <div class="card-body">
                    <p><i class="fas fa-street-view"></i><b><?php echo " " . $items->first_address; ?></b></p>
                    <p><i class="fas fa-map-marker-alt"></i><?php echo " " . $items->last_address; ?></p>
                    <p style="color:red;font-size: 18px;"><i class="fas fa-tenge"></i> <?php echo "  " . $items->price; ?></p>
                    <button type="button" order-id="<?php echo $items->id; ?>" user-id="<?php echo $_SESSION['user_id']; ?>" id="btnGetOrder" class="btn btn-success stretched-link">Принять</button>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<input type="hidden" id="transport-added" value="<?php echo $data['isTransport']; ?>" />
<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>
    var transportAdded = $('#transport-added').val();
    if (transportAdded != 1) {
        toastr["warning"]("Вы еще не добавили транспорт<br /><br /><a href='/driver/transport' class='btn btn-primary'>Добавить</a>")

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "showDuration": "300",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "onclick": "/transport"
        }
    }

    $("#btnGetOrder").on("click", function(e) {
        var order_id = $(this).attr("order-id");
        var user_id = $(this).attr("user-id");
        var postData = {
            'order_id': order_id,
            'user_id': user_id
        };
        console.log("order id = " + order_id + "\nuser id = " + user_id);
        $.ajax({
            url: "<?php echo URLROOT ?>/driver/acceptOrder/",
            type: "post",
            data: postData,
            success: function(response) {
                console.log(response)
            },
            error: function(error) {
                alert(error);
            }
        });
    });
</script>