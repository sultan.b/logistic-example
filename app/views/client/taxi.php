<?php require APPROOT . '/views/inc/header.php'; ?>
<style>
  #map {
    height: 100%;
    margin-top: -1rem;
  }

  /* Optional: Makes the sample page fill the window. */
  html,
  body {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  #origin-input,
  #destination-input,
  #comment-input,
  #price-input {
    background-color: #fff;
    font-family: Roboto;
    font-weight: 300;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 300px;
  }

  #origin-input:focus,
  #destination-input:focus {
    border-color: #4d90fe;
  }
</style>
<?php if ($_GET['act'] == 'history') : ?>
  <div class="container">
    <div class="row">
      <div class="card w-100">
        <div class="card-body">
          <a href="/client/taxi">Назад</a>
          <h4 class="card-title">История заказов такси</h4>
          <div class="card-text">
            <?php if ($data['history'] != null) : ?>
              <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Водитель</th>
                    <th scope="col">Транспорт</th>
                    <th scope="col">Гос номер</th>
                    <th scope="col">Откуда</th>
                    <th scope="col">Куда</th>
                    <th scope="col">Описание</th>
                    <th scope="col">Статус</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data['history'] as $items) : ?>
                    <tr>
                      <td><?php echo $items->f_name . " " . $items->l_name; ?></th>
                      <td><?php echo $items->t_name; ?> </td>
                      <td><?php echo $items->t_state_number; ?> </td>
                      <td><?php echo $items->first_address; ?> </td>
                      <td><?php echo $items->last_address; ?></td>
                      <td><?php echo $items->comment; ?></td>
                      <td>
                        <?php if ($items->is_finished == 1) : ?>
                          <p class="text-success">Выполено</p>
                        <?php elseif ($items->is_accepted != 1) : ?>
                          <p class="text-danger">Заказ еще не приянто</p>
                        <?php endif; ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            <?php else : ?>
              <div class="alert alert-info">У вас еще нет заказов</div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php else : ?>
  <div class="card" id="card" style="position: absolute;top: 100px;z-index: 1;left: 40px;opacity: 0.85;">
    <div class="card-body">
      <h5 class="card-title text-center">Заказать такси</h5>
      <form action="" method="POST">
        <input type="hidden" name="order_type_id" value="1" />
        <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']; ?>" />
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <span class="input-group-text">A</span>
          </div>
          <input id="origin-input" name="first_address" class="form-control" type="text" placeholder="Откуда">
        </div>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <span class="input-group-text">B</span>
          </div>
          <input id="destination-input" name="last_address" class="form-control" type="text" placeholder="Куда">
        </div>
        <div class="input-group mb-2">
          <div class="input-group-prepend">
            <span class="input-group-text">₸</span>
          </div>
          <input id="price-input" name="price" class="form-control" type="text" placeholder="Введите цену">
        </div>
        <div class="input-group mb-2">
          <textarea id="comment-input" class="form-control" name="comment" rows="3" placeholder="Комментарий, пожелания"></textarea>
        </div>
      </form>
      <div class="row">
        <div class="col">
          <input type="submit" id="btnOrder" value="Заказать" class="btn btn-success btn-block">
        </div>
        <div class="col">
          <a href="/client/taxi?act=history" class="btn btn-success btn-block">История заказов</a>
        </div>
      </div>
    </div>
  </div>
  <div id="map"></div>
  <!-- <div class="loader"></div> -->
  <span class="pulse" style="display: none;"></span>
<?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>
<script>
  var map;

  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      mapTypeControl: false,
      center: {
        lat: 51.1636836,
        lng: 71.4468956
      },
      zoom: 13
    });

    new AutocompleteDirectionsHandler(map);
  }

  /**
   * @constructor
   */
  function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'DRIVING';
    this.directionsService = new google.maps.DirectionsService;
    this.directionsRenderer = new google.maps.DirectionsRenderer;
    // this.directionsRenderer = new google.maps.DirectionsRenderer({
    //   preserveViewport: true
    // });
    this.directionsRenderer.setMap(map);

    var card = document.getElementById('card');

    var originInput = document.getElementById('origin-input');
    var destinationInput = document.getElementById('destination-input');
    var modeSelector = document.getElementById('mode-selector');

    var originAutocomplete = new google.maps.places.Autocomplete(originInput);
    // Specify just the place data fields that you need.
    originAutocomplete.setFields(['place_id']);

    var destinationAutocomplete =
      new google.maps.places.Autocomplete(destinationInput);
    // Specify just the place data fields that you need.
    destinationAutocomplete.setFields(['place_id']);

    this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
    this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');
    //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);
    //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
    //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(
    //  destinationInput);
  }


  AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(
    autocomplete, mode) {
    var me = this;
    autocomplete.bindTo('bounds', this.map);
    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.place_id) {
        window.alert('Please select an option from the dropdown list.');
        return;
      }
      if (mode === 'ORIG') {
        me.originPlaceId = place.place_id;
      } else {
        me.destinationPlaceId = place.place_id;
      }
      me.route();
    });
  };

  AutocompleteDirectionsHandler.prototype.route = function() {
    if (!this.originPlaceId || !this.destinationPlaceId) {
      return;
    }
    var me = this;

    this.directionsService.route({
        origin: {
          'placeId': this.originPlaceId
        },
        destination: {
          'placeId': this.destinationPlaceId
        },
        travelMode: 'DRIVING'
      },
      function(response, status) {
        if (status === 'OK') {
          me.directionsRenderer.setDirections(response);

        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
  };

  $('#btnOrder').click(function() {
    $.ajax({
      url: "<?php echo URLROOT ?>/client/taxi/",
      type: "post",
      data: $("form").serialize(),
      success: function(response) {
        console.log("resopnce" + response);
        var data = JSON.parse(response);
        $("#btnOrder").hide();
        getAcceptedOrder(data[0].order_id);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        console.log("Ошибка" + textStatus);
      }
    });
  });

  function getAcceptedOrder(id) {
    var acceptedId;
    $.ajax({
      url: '<?php echo URLROOT ?>/client/getAcceptedOrder/' + id,
      success: function(data) {
        console.log("DATA = " + data);
        var res = JSON.parse(data);

        if (res[0].accepted != null) {
          console.log("is_accepted = " + res[0].accepted.is_accepted);
          acceptedId = res[0].accepted.is_accepted;
          window.location = "<?php echo URLROOT ?>/client/currentOrder/" + res[0].accepted.id;
          var forClient = {
            'acceptedOrder': res[0].accepted.id,
            'order_id': res[0].accepted.client_order_id,
            'driver_id': res[0].accepted.driver_id,
            'user_id': res[0].accepted.user_id,
          };
          $.ajax({
            url: '<?php echo URLROOT ?>/client/currentOrder',
            data: forClient,
            success: function(data) {

            }
          });
        } else {
          $(".pulse").css({
            display: "block"
          });
          setInterval(getAcceptedOrder(id), 5000);
        }
      }
    });
    return acceptedId;
  }
</script>