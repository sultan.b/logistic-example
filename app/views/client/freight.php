<?php require APPROOT . '/views/inc/header.php'; ?>
<?php if (isLoggedIn()) : ?>
    <div class="container">
        <div class="row">
            <?php if ($_GET['act'] == 'history') : ?>
                <div class="card w-100">
                    <div class="card-body">
                        <a href="/client/freight">Назад</a>
                        <h4 class="card-title">История заказов грузоперевозок</h4>
                        <div class="card-text">
                            <?php if ($data['freightOrders'] != null) : ?>
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Водитель</th>
                                            <th scope="col">Откуда</th>
                                            <th scope="col">Куда</th>
                                            <th scope="col">Описание</th>
                                            <th scope="col">Статус</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data['freightOrders'] as $items) : ?>
                                            <tr>
                                                <td><?php echo $items->f_name . " " . $items->l_name; ?></th>
                                                <td><?php echo $items->first_address; ?> </td>
                                                <td><?php echo $items->last_address; ?></td>
                                                <td><?php echo $items->comment; ?></td>
                                                <td>
                                                    <?php if ($items->is_finished == 1) : ?>
                                                        <p class="text-success">Выполено</p>
                                                    <?php elseif ($items->is_accepted != 1) : ?>
                                                        <p class="text-danger">Заказ еще не приянто</p>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php else : ?>
                                <div class="alert alert-info">У вас еще нет заказов</div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <div class="card w-100">
                    <div class="card-body">
                        <div class="float-right"><a href="/client/freight?act=history">История заказов</a></div>
                        <h4 class="card-title text-center">Грузоперевозки</h4>
                        <div class="card-text">
                            <div id="map" class="mb-3" style="width:100%;height:400px;"></div>
                            <form action="" method="POST">
                                <input type="hidden" name="order_type_id" value="2" />
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">A</span>
                                    </div>
                                    <input name="first_address" id="first_address" class="form-control" type="text" placeholder="Откуда">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">B</span>
                                    </div>
                                    <input name="last_address" id="last_address" class="form-control" type="text" placeholder="Куда">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₸</span>
                                    </div>
                                    <input name="price" class="form-control" type="text" placeholder="Стоимость">
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-calendar-alt" style="font-size: 1rem;"></i></span>
                                    </div>
                                    <input name="start_date" id="st_date" class="form-control" type="text" placeholder="Дата и время">
                                    <!-- <input name="start_date" type="hidden" value="" /> -->
                                </div>
                                <div class="input-group mb-2">
                                    <textarea class="form-control" name="comment" rows="3" placeholder="Размеры и тип груза (Укажите более точную информацию для водителя)"></textarea>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col">
                                    <input type="submit" id="btnFreightOrder" value="Заказать грузовое авто" class="btn btn-success btn-block">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    </div>
<?php endif; ?>
<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>
    $("#btnFreightOrder").click(function() {
        $.ajax({
            url: "<?php echo URLROOT ?>/client/freight/",
            type: "post",
            data: $("form").serialize(),
            success: function(response) {
                console.log("resopnce = " + response);
                var data = JSON.parse(response);
                console.log("data = " + data);
                $("#btnFreightOrder").hide();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
    });

    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {
                lat: 51.1636836,
                lng: 71.4468956
            },
            zoom: 13
        });

        new AutocompleteDirectionsHandler(map);
    }

    /**
     * @constructor
     */
    function AutocompleteDirectionsHandler(map) {
        this.map = map;
        this.originPlaceId = null;
        this.destinationPlaceId = null;
        this.travelMode = 'DRIVING';
        this.directionsService = new google.maps.DirectionsService;
        this.directionsRenderer = new google.maps.DirectionsRenderer;
        // this.directionsRenderer = new google.maps.DirectionsRenderer({
        //   preserveViewport: true
        // });
        this.directionsRenderer.setMap(map);

        var card = document.getElementById('card');

        var originInput = document.getElementById('first_address');
        var destinationInput = document.getElementById('last_address');
        var modeSelector = document.getElementById('mode-selector');

        var originAutocomplete = new google.maps.places.Autocomplete(originInput);
        // Specify just the place data fields that you need.
        originAutocomplete.setFields(['place_id']);

        var destinationAutocomplete =
            new google.maps.places.Autocomplete(destinationInput);
        // Specify just the place data fields that you need.
        destinationAutocomplete.setFields(['place_id']);

        this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
        this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(
        //  destinationInput);
    }


    AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(
        autocomplete, mode) {
        var me = this;
        autocomplete.bindTo('bounds', this.map);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.place_id) {
                window.alert('Please select an option from the dropdown list.');
                return;
            }
            if (mode === 'ORIG') {
                me.originPlaceId = place.place_id;
            } else {
                me.destinationPlaceId = place.place_id;
            }
            me.route();
        });
    };

    AutocompleteDirectionsHandler.prototype.route = function() {
        if (!this.originPlaceId || !this.destinationPlaceId) {
            return;
        }
        var me = this;

        this.directionsService.route({
                origin: {
                    'placeId': this.originPlaceId
                },
                destination: {
                    'placeId': this.destinationPlaceId
                },
                travelMode: 'DRIVING'
            },
            function(response, status) {
                if (status === 'OK') {
                    me.directionsRenderer.setDirections(response);

                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
    };

    $("#st_date").datetimepicker({
        format: "yyyy-mm-dd hh:ii",
        autoclose: true,
        todayBtn: true,
        startDate: new Date(),
    });
</script>