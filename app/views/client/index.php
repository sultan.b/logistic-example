<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="card">
                <img src="<?php echo IMGSRC ?>/tax.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h4 class="card-title">Такси</h4>
                    <a href="/client/taxi" class="btn btn-primary stretched-link">Заказать</a>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <img src="<?php echo IMGSRC ?>/freight1.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h4 class="card-title">Грузоперевозки</h4>
                    <a href="/client/freight" class="btn btn-primary stretched-link">Заказать</a>
                </div>
            </div>
        </div>
        <div class="col-3">
        <div class="card">
            <img src="<?php echo IMGSRC ?>/spec.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h4 class="card-title">Спецтехники</h4>
                <a href="/client/other" class="btn btn-primary stretched-link">Заказать</a>
            </div>
        </div>
        </div>
        
        <div class="col-3">
            <div class="card">
                <img src="<?php echo IMGSRC ?>/bus.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h4 class="card-title">Автобусы</h4>
                    <a href="/client/bus" class="btn btn-primary stretched-link">Заказать</a>
                </div>
            </div>
        </div>
        </div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>