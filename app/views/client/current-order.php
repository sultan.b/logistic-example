<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php if (isLoggedIn() && $data['order'] != null) : ?>
            <div class="col">
                <div class="card text-black m-2 shadow">
                    <h3 class="card-header bg-warning text-white">Текущий заказ</h3>
                    <div class="card-body">
                        <!-- <h6 class="card-subtitle mb-2 text-muted">Информация о текущем заказе</h6> -->
                        <div style="font-size:20px;"><i class="fas fa-map-marker-alt"></i> <?php echo $data['order']->first_address; ?></div>
                        <div style="font-size:20px;"><i class="fas fa-flag-checkered"></i> <?php echo $data['order']->last_address; ?></div><br>
                        <div style="font-size:30px;font-weight: 700;"><i class="fas fa-tenge"></i> <?php echo $data['order']->price; ?></div>
                        <button class="btn btn-primary mt-3">Отменить заказ</button>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="flex">
                    <div class="card m-2 shadow">
                        <h4 class="card-header bg-warning text-white">Водитель</h4>
                        <div class="card-body">
                            <div>
                                <i class="fas fa-user-tie"></i> ФИО: <?php echo $data['order']->f_name . ' ' . $data['order']->l_name; ?>
                            </div>
                            <div>
                                <i class="fas fa-phone-alt"></i> Телефон: <a href="tel:<?php echo $data['order']->phone; ?>"><?php echo $data['order']->phone; ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="card shadow m-2">
                        <h4 class="card-header bg-warning text-white">Транспорт</h4>
                        <div class="card-body">
                            <div>
                                <i class="fas fa-car"></i> Название: <?php echo $data['order']->t_name; ?>
                            </div>
                            <div>
                                Гос. номер: <?php echo $data['order']->t_state_number; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>