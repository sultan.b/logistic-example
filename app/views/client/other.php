<?php require APPROOT . '/views/inc/header.php'; ?>

<div class="container">
    <?php if (isLoggedIn()) : ?>
        <div class="row">
            <?php if ($_GET['act'] == 'history') : ?>
                <div class="card w-100">
                    <div class="card-body">
                        <a href="/client/other">Назад</a>
                        <h4 class="card-title">История заказов спецтехники</h4>
                        <?php if ($data['history'] != null) : ?>
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Водитель</th>
                                        <th scope="col">Транспорт</th>
                                        <th scope="col">Наименование работ</th>
                                        <th scope="col">Место</th>
                                        <th scope="col">Дата</th>
                                        <th scope="col">Описание</th>
                                        <th scope="col">Статус</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data['history'] as $items) : ?>
                                        <tr>
                                            <td><?php echo $items->f_name . " " . $items->l_name; ?></th>
                                            <td><?php echo $items->transport_name; ?></td>
                                            <td><?php echo $items->work_title; ?></td>
                                            <td><?php echo $items->first_address; ?> </td>
                                            <td><?php echo $items->start_date; ?></td>
                                            <td><?php echo $items->comment; ?></td>
                                            <td>
                                                <?php if ($items->is_finished == 1) : ?>
                                                    <p class="text-success">Выполено</p>
                                                <?php elseif($items->is_accepted != 1): ?>
                                                    <p class="text-danger">Заказ еще не приянто</p>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php else : ?>
                            <div class="alert alert-info">У вас еще нет заказов</div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php else : ?>
                <div class="card w-100">
                    <div class="card-body">
                        <div class="float-right"><a href="/client/other?act=history">История заказов</a></div>
                        <h4 class="card-title">Спецтехника</h4>
                        <div class="card-text">
                            <div id="otherBookingTab" class="container tab-pane active"><br>
                                <div class="select-subtype">
                                    <H4>Выберите тип спецтехники</H4>
                                    <ul class="list-group">
                                        <?php foreach ($data['subtypes'] as $items) : ?>
                                            <a href="#" class="list-group-item list-group-item-action" subtype-id="<?php echo $items->id; ?>"><?php echo $items->transport_name; ?></a>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="other-order" style="display: none;">
                                    <div id="map" class="mb-3" style="width:100%;height:400px;"></div>
                                    <form action="" method="POST">
                                        <input type="hidden" name="order_subtype_id" id="order_subtype_id" value="" />
                                        <input type="hidden" name="order_type_id" value="3" />
                                        <h5>Заказать <b><span id="subtypeTitle"></span></b></h5>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-briefcase"></i></span>
                                            </div>
                                            <input name="work_title" class="form-control" type="text" placeholder="Наименование работ">
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-map-marker-alt fa-lg"></i></span>
                                            </div>
                                            <input id="first_address" name="first_address" class="form-control" type="text" placeholder="Где будут проходить работы?">
                                        </div>
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-tenge fa-lg"></i></span>
                                            </div>
                                            <input name="price" class="form-control" type="text" placeholder="Цена">
                                        </div>
                                        <div class="input-group mb-2">
                                            <textarea class="form-control" name="comment" rows="3" placeholder="Комментарий"></textarea>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col">
                                            <input type="submit" id="btnOtherOrder" value="Заказать" class="btn btn-success btn-block">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php else : ?>
        <div class="row">
            <div class="alert alert-danger w-100">Вы не авторизованы</div>
        </div>
    <?php endif; ?>
</div>


<?php require APPROOT . '/views/inc/footer.php'; ?>

<script>
    $(".select-subtype a").click(function() {
        var subtypeId = $(this).attr("subtype-id")
        $(".select-subtype").hide();
        $(".other-order").show();
        $("#subtypeTitle").text($(this).text());
        $("#order_subtype_id").val(subtypeId);
    });

    $("#btnOtherOrder").click(function() {
        $.ajax({
            url: "<?php echo URLROOT ?>/client/other/",
            type: "post",
            data: $("form").serialize(),
            success: function(response) {
                console.log("resopnce = " + response);
                var data = JSON.parse(response);
                console.log("data = " + data);
                $("#btnOtherOrder").hide();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Ошибка" + textStatus);
            }
        });
    });

    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {
                lat: 51.1636836,
                lng: 71.4468956
            },
            zoom: 13
        });

        new AutoCompleteMarker(map);
    }

    /**
     * @constructor
     */
    function AutoCompleteMarker(map) {
        this.map = map;

        var input = document.getElementById('first_address');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });


        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
            }
            marker.setIcon( /** @type {google.maps.Icon} */ ({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);


            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            //infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.setContent('<div><strong>' + place.name + '</strong><br>');
            infowindow.open(map, marker);
        });
    };
</script>