<?php require APPROOT . '/views/inc/header.php'; ?>

<div class="container">
    <div id="pages-about-jumbotron" class="jumbotron text-center py-4 mb-3">
        <h1 class="text-white jumbotron-text-shadow">Вопросы и ответы</h1>
    </div>
</div>

<div class="container pt-2 pb-2 mb-2">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                <h5 class="card-title">Клиент</h5>
                    <div class="card-text">
                        <div id="accordin" role="tablist" aria-multiselectable="true">
                            <?php $i = 0; foreach ($data['clientFaq'] as $items) : ?>
                                <div class="card mb-1">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapse<?php echo $i; ?>">
                                            <?php echo $items->question; ?>
                                        </a>
                                    </div>
                                    <div id="collapse<?php echo $i; ?>" class="collapse" data-parent="#accordin">
                                        <div class="card-body">
                                            <?php echo $items->answer; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php $i++; endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Водитель</h5>
                    <div class="card-text">
                        <div id="accordin" role="tablist" aria-multiselectable="true">
                            <?php $i = 0; foreach ($data['driverFaq'] as $items) : ?>
                                <div class="card mb-1">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseDriver<?php echo $i; ?>">
                                            <?php echo $items->question; ?>
                                        </a>
                                    </div>
                                    <div id="collapseDriver<?php echo $i; ?>" class="collapse" data-parent="#accordin">
                                        <div class="card-body">
                                            <?php echo $items->answer; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php $i++; endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>