<?php require APPROOT .'/views/inc/header.php'; ?>
<!-- Jumbotron -->
<div class="container">    
    <div id="pages-contact-jumbotron" class="jumbotron text-center py-4 mb-3">
        <h1 class="text-white jumbotron-text-shadow">Контакты</h1>
    </div>
</div>
<!-- Contact -->
<section class="container mt-4 mb-5 pb-5">
    <div class="row">
        <!--Grid column-->
        <div class="col-lg-5 mb-4">
            <!--Form with header-->
            <form method="POST" action="">
            	<div class="card card-block rounded-0 text-dark">
	                <div class="card-header p-0">
	                    <div class="text-center py-3">
	                        <h3 class="text-dark"><i class="fa fa-envelope"></i> Напишите нам</h3>
	                    </div>
	                </div>
	                <div class="card-body p-3">
	                    <!--Body-->
	                    <div class="form-group">
	                        <label class="control-label">Ваше имя:</label>
	                        <div class="input-group">
	                            <div class="input-group-prepend">
	                            	<span class="input-group-text"><i class="fa fa-user"></i></span>
	                            </div>
	                            <input required type="text" class="form-control" id="inlineFormInputGroupUsername" placeholder="Имя">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label">Ваш email:</label>
	                        <div class="input-group mb-2 mb-sm-0">
	                            <div class="input-group-prepend">
	                            	<span class="input-group-text"><i class="fa fa-envelope"></i></span>
	                            </div>
	                            <input required type="email" class="form-control" id="inlineFormInputGroupUsername" placeholder="Email">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label">Сообщение:</label>
	                        <div class="input-group mb-2 mb-sm-0">
	                            <div class="input-group-prepend">
	                            	<span class="input-group-text"><i class="fa fa-pencil"></i></span>
	                            </div>
	                            <textarea required placeholder="Сообщение" class="form-control"></textarea>
	                        </div>
	                    </div>
	                    <div class="text-center">
	                        <button class="btn btn-dark btn-block rounded-0 py-2">Отправить</button>
	                    </div>
	                </div>
	            </div>
            </form>
            <!--Form with header-->
        </div>
        <div class="col-lg-7">
            <!--Google map-->
            <div class="mb-4">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A60ad1c52dee8253557e4ff3f2bc958b622ce34517bce649eb7bfdbaa54d428be&amp;width=100%25&amp;height=450&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
            <!--Buttons-->
            <div class="row text-dark">
                <div class="col-md-12">
                    <p class="bg-dark px-3 py-2 rounded text-white mb-2 d-inline-block"><i class="fa fa-map-marker"></i></p>
                    <p>Нур-Султан, Монумент Байтерек,<br> Казахстан</p>
                </div>
                </div>
            </div>
        </div>
       <!--Grid column-->

    </div>

</section>
<!-- Contact -->
<?php require APPROOT .'/views/inc/footer.php'; ?>