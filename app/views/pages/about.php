<?php require APPROOT . '/views/inc/header.php'; ?>
<!-- Jumbotron -->
<div class="container">
    <div id="pages-about-jumbotron" class="jumbotron text-center py-4 mb-3">
        <h1 class="text-white jumbotron-text-shadow">О нас</h1>
    </div>
</div>
<div class="container pt-2 pb-2 mb-2">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title text-dark text-center">Наша миссия</h2>
                    <div class="card-text text-dark text-center">
                        <p>Мы возвращаем людям самое ценное — свободу выбора. Мы связываем их напрямую, потому что знаем, как важно иметь возможность самостоятельно принимать решения. Каждый день мы защищаем миллионы людей от диктата и манипуляций глобальных корпораций, навязывающих свои условия.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container pt-2 pb-2 mb-2">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-block">
                <div class="card-body">
                    <h2 class="card-title text-dark">Наши ценности:</h2>
                    <div class="card-text text-dark">
                        <div class="pull-left">
                            <div style="margin-top: 8rem;">
                                <li>Мы приносим пользу</li>
                                <li>Мы вдохновляем на личностный и профессиональный рост</li>
                                <li>Мы самостоятельны, инициативны и неравнодушны</li>
                                <li>Мы больше чем команда, уважаем и заботимся друг о друге</li>
                                <li>Мы честные, точные, открытые и ответственные</li>
                                <li>Мы достигаем большего меньшими средствами, действуя эффективно</li>
                                <li>Мы нацелены на результат</li>
                            </div>
                        </div>
                        <div class="pull-right">
                            <img src="<?php echo IMGSRC; ?>001.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>