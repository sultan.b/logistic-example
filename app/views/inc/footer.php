<footer class="page-footer bg-dark text-white">
    <div class="container">
        <div class="row pt-5 mb-3 text-center d-flex justify-content-center">
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a class="text-white" href="<?php echo URLROOT ?>">Главная</a></h6>
            </div>
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a class="text-white" href="<?php echo URLROOT ?>/pages/about">О нас</a></h6>
            </div>
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a class="text-white" href="<?php echo URLROOT ?>/pages/faq">Вопросы и ответы</a></h6>
            </div>
            <div class="col-md-2 mb-3">
                <h6 class="title font-bold"><a class="text-white" href="<?php echo URLROOT ?>/pages/contact">Контакты</a></h6>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-md-12">
                <div class="mb-2 text-center">
                    <!--Facebook-->
                    <a href="#" class="icons-sm text-white fb-ic"><i class="fab fa-facebook fa-lg white-text mr-md-4"> </i></a>
                    <!--Twitter-->
                    <a href="#" class="icons-sm text-white tw-ic"><i class="fab fa-twitter fa-lg white-text mx-md-4"> </i></a>
                    <!--Instagram-->
                    <a href="#" class="icons-sm text-white ins-ic"><i class="fab fa-instagram fa-lg white-text mx-md-4"> </i></a>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="container-fluid text-center pb-3">
            © 2020 Copyright: <a class="text-white" href="<?php echo URLROOT ?>"> Логистика </a>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/stickyFooter.js"></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/main.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmPhhL21vzJ-pVaxXrpmJqMv7D2HrEDU8&libraries=places&callback=initMap"
    async defer></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/inputmask.js"></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/toastr.js"></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/all.min.js"></script>
<script type="text/javascript" src="<?php echo URLROOT ?>/js/fontawesome.min.js"></script>
</body>
</html>