<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="id=edge">
	<title><?php echo SITENAME; ?></title>
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
	<!-- <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/superhero/bootstrap.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/flatly/bootstrap.min.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/bootstrap-grid.css">
	<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/toastr.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/bootstrap-datetimepicker.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URLROOT; ?>/css/fontawesome.min.css">
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="<?php echo URLROOT ?>/js/bootstrap.min.js"></script>
</head>

<body>
	<?php require APPROOT . '/views/inc/navbar.php'; ?>