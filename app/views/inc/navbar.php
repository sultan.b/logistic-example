<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
  <div class="container">
    <a class="navbar-brand" href="<?php echo URLROOT; ?>"><?php echo SITENAME; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URLROOT; ?>">Главная</a>
        </li>
        <?php if ($_SESSION['driver']) : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/driver">Заказы</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/driver/orders">История заказов</a>
          </li>
        <?php endif; ?>
        <?php if ($_SESSION['client']) : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/client">Заказать</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="historyDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              История заказов
            </a>
            <div class="dropdown-menu" aria-labelledby="historyDropdown">
              <a class="dropdown-item" href="/client/taxi?act=history">Такси</a>
              <a class="dropdown-item" href="/client/freight?act=history">Грузоперевозки</a>
              <a class="dropdown-item" href="/client/other?act=history">Спецтехники</a>
              <a class="dropdown-item" href="/client/bus?act=history">Автобусы</a>
            </div>
          </li>
        <?php endif; ?>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URLROOT; ?>/pages/about">О нас</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URLROOT; ?>/pages/faq">Вопросы и ответы</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URLROOT; ?>/pages/contact">Контакты</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <?php if (isset($_SESSION['admin_mode'])) { ?>
          <li class="nav-item">
            <a class="btn btn-primary" href="<?php echo URLROOT; ?>/admin">Админ панель</a>
          </li>
        <?php } ?>
        <?php if (isset($_SESSION['user_id'])) : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/profile"><?php echo $_SESSION['user_name'] ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout">Выйти</a>
          </li>
        <?php else : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/register">Регистрация</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/login">Авторизация</a>
          </li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>