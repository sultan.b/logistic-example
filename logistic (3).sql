-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 28 2020 г., 12:41
-- Версия сервера: 10.3.13-MariaDB
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `logistic`
--

-- --------------------------------------------------------

--
-- Структура таблицы `client_orders`
--

CREATE TABLE `client_orders` (
  `id` int(11) NOT NULL,
  `order_type_id` int(11) NOT NULL,
  `order_subtype_id` int(11) DEFAULT NULL,
  `first_address` varchar(2500) DEFAULT NULL,
  `last_address` varchar(2500) DEFAULT NULL,
  `work_title` varchar(5000) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `comment` varchar(5000) DEFAULT NULL,
  `requirements_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `is_accepted` tinyint(1) NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `client_orders`
--

INSERT INTO `client_orders` (`id`, `order_type_id`, `order_subtype_id`, `first_address`, `last_address`, `work_title`, `price`, `comment`, `requirements_id`, `user_id`, `is_accepted`, `start_date`, `created_at`, `updated_at`) VALUES
(52, 1, NULL, 'Манаса 9, Нур-Султан, Казахстан', 'проспект Абая 8, Нур-Султан, Казахстан', NULL, 700, '', NULL, 5, 1, NULL, '2020-03-24 11:59:49', '2020-03-24 11:59:45'),
(53, 3, 9, 'цукцук', NULL, NULL, NULL, 'цкцук', NULL, 5, 0, NULL, '2020-03-26 08:52:36', '2020-03-26 08:52:36'),
(54, 3, 9, 'test', NULL, NULL, NULL, '1234', NULL, 5, 0, NULL, '2020-03-26 09:10:39', '2020-03-26 09:10:39'),
(55, 4, 12, 'Нур-Султан, Казахстан', 'Бурабай, Казахстан', NULL, 20000, 'нас 6 человек', NULL, 5, 0, NULL, '2020-03-28 06:21:41', '2020-03-28 06:21:41'),
(56, 2, NULL, 'проспект Абылай хан 5, Нур-Султан, Казахстан', 'улица Абая 87, Кокшетау, Казахстан', NULL, 30000, '', NULL, 5, 0, NULL, '2020-03-28 06:39:49', '2020-03-28 06:39:49'),
(57, 2, NULL, 'Манаса 9, Нур-Султан, Казахстан', 'улица Кенесары 69, Нур-Султан, Казахстан', NULL, 3000, 'тест', NULL, 5, 0, NULL, '2020-03-28 06:44:16', '2020-03-28 06:44:16');

-- --------------------------------------------------------

--
-- Структура таблицы `drivers`
--

CREATE TABLE `drivers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `transport_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `drivers`
--

INSERT INTO `drivers` (`id`, `user_id`, `transport_id`) VALUES
(1, 4, 16);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `from_place` int(11) NOT NULL,
  `to_place` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT 0,
  `truck_id` int(11) DEFAULT NULL,
  `address` varchar(300) NOT NULL,
  `Note` text NOT NULL,
  `mailed` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `requirements`
--

CREATE TABLE `requirements` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `transport`
--

CREATE TABLE `transport` (
  `id` int(11) NOT NULL,
  `t_name` varchar(2500) DEFAULT NULL,
  `t_type_id` int(11) NOT NULL,
  `t_color_id` int(11) DEFAULT NULL,
  `t_state_number` varchar(2500) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subtype_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transport`
--

INSERT INTO `transport` (`id`, `t_name`, `t_type_id`, `t_color_id`, `t_state_number`, `user_id`, `subtype_id`, `created_at`, `updated_at`) VALUES
(16, 'K-700', 3, NULL, '001AAA01', 4, 19, '2020-03-11 04:33:21', '2020-03-11 04:33:21');

-- --------------------------------------------------------

--
-- Структура таблицы `transport_subtype`
--

CREATE TABLE `transport_subtype` (
  `id` int(11) NOT NULL,
  `transport_name` varchar(500) DEFAULT NULL,
  `transport_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transport_subtype`
--

INSERT INTO `transport_subtype` (`id`, `transport_name`, `transport_type_id`) VALUES
(2, 'Газель', 2),
(3, 'КамАЗ', 2),
(9, 'Трактор', 3),
(11, 'VIP автобусы', 4),
(12, 'Микроватобусы', 4),
(13, 'Автобусы', 4),
(14, 'Бульдозер', 3),
(15, 'Кран', 3),
(18, 'Эксковатр', 3),
(19, 'Погрузчик', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `transport_types`
--

CREATE TABLE `transport_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transport_types`
--

INSERT INTO `transport_types` (`id`, `name`) VALUES
(1, 'Легковые'),
(2, 'Грузоперевозки'),
(3, 'Спецтехника'),
(4, 'Автобусы');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `avatar` longblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `email`, `password`, `phone`, `address`, `user_role_id`, `is_admin`, `avatar`) VALUES
(1, 'Beibarys', 'Sultan', 'admin@mail.ru', '$2y$10$uiXtjOiwcuGaQkmG159iHOwSxIiFLJ0LA9dN5dbSyVnRyDP/HY2/e', '+7 (747) 244-5696', 'Astana, Kazakhstan', NULL, 1, NULL),
(4, 'test', 'testt', 'test@mail.com', '$2y$10$kw/BWWT/pRVSXML9fsb/NOXij/rSmAIgjypEBRMsHm8eWb.z2ABgq', '+7 (677) 777-7777', NULL, 5, NULL, NULL),
(5, 'Client', 'Client', 'client@mail.ru', '$2y$10$qtGI2N.URiqYuQxkhBlheug1S5r3MgcUMHrprSeZbdTiLuUewEXDm', '+7 (777) 123-3333', NULL, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users_types`
--

CREATE TABLE `users_types` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_types`
--

INSERT INTO `users_types` (`id`, `type`) VALUES
(1, 'admin'),
(2, 'employee'),
(3, 'driver');

-- --------------------------------------------------------

--
-- Структура таблицы `user_accepted_orders`
--

CREATE TABLE `user_accepted_orders` (
  `id` int(11) NOT NULL,
  `client_order_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `is_accepted` tinyint(1) DEFAULT NULL,
  `is_started` tinyint(1) DEFAULT NULL,
  `is_finished` tinyint(1) DEFAULT NULL,
  `is_waited` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_accepted_orders`
--

INSERT INTO `user_accepted_orders` (`id`, `client_order_id`, `driver_id`, `is_accepted`, `is_started`, `is_finished`, `is_waited`) VALUES
(22, 52, 4, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL,
  `role_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`) VALUES
(5, 'Водитель'),
(6, 'Клиент');

-- --------------------------------------------------------

--
-- Структура таблицы `user_transport`
--

CREATE TABLE `user_transport` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `transport_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `client_orders`
--
ALTER TABLE `client_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `driver_id` (`driver_id`),
  ADD KEY `truck_id` (`truck_id`),
  ADD KEY `from_place` (`from_place`),
  ADD KEY `to_place` (`to_place`);

--
-- Индексы таблицы `requirements`
--
ALTER TABLE `requirements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transport_subtype`
--
ALTER TABLE `transport_subtype`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transport_types`
--
ALTER TABLE `transport_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users_types`
--
ALTER TABLE `users_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_accepted_orders`
--
ALTER TABLE `user_accepted_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_transport`
--
ALTER TABLE `user_transport`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `client_orders`
--
ALTER TABLE `client_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT для таблицы `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `requirements`
--
ALTER TABLE `requirements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `transport`
--
ALTER TABLE `transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `transport_subtype`
--
ALTER TABLE `transport_subtype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `transport_types`
--
ALTER TABLE `transport_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users_types`
--
ALTER TABLE `users_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `user_accepted_orders`
--
ALTER TABLE `user_accepted_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT для таблицы `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `user_transport`
--
ALTER TABLE `user_transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
